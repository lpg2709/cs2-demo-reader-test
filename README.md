# cs2-demo-reader-test

Read cs2 demo files.

- [Zig doc](https://ziglang.org/documentation/0.12.0/)
- [Zig Standard Library](https://ziglang.org/documentation/0.12.0/std/)
- [Demo file - VOLVO](https://developer.valvesoftware.com/wiki/DEM_\(file_format\))

## Usage

Save the demo file inside folder `demo`.

Run with

```
zig build run
```

