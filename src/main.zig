const std = @import("std");
const fs = std.fs;
const log = std.log;

const DemoHeader = struct {
    header: *[8]u8,
    demo_protocol: i32,
    network_protocol: i32,
    server_name: []u8,
    client_name: []u8,
    //     map_name: *[260]u8,
    //     game_directory: *[260]u8,
    //     playback_time: f64,
    //     ticks: i32,
    //     frames: i32,
    //     sign: i32,
    pub fn print(self: DemoHeader) void {
        std.debug.print(
            \\--- HEADER ---
            \\- header: {s}
            \\- Demo protocol: {d}
            \\- Network protocol: {d}
            \\- Server name: {s}
            \\- Client name: {s}
        , .{ self.header, self.demo_protocol, self.network_protocol, self.server_name, self.client_name });
    }
};

fn readString(buf: []u8, start: usize, max: usize) []u8 {
    var end = max;
    for (start..(start + max)) |i| {
        const c: u8 = buf[i];
        if (c == 0) {
            end = i;
            break;
        }
    }
    return buf[start..end];
}

fn readInt(buf: []u8, start: usize) i32 {
    return std.mem.readVarInt(i32, buf[start..(start + 4)], .little);
}

pub fn main() !void {
    const argv = std.os.argv;
    const argc = argv.len;
    if (argc < 2) {
        log.err("Invalid argument.\nUsage: ./cs2-demo-reader-test <demo-path>\n", .{});
        return;
    }
    const file_path = std.mem.span(argv[1]);

    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    std.debug.print("ARGS ({d}): \n", .{argc});
    for (0.., argv) |i, e| {
        std.debug.print("-- {d}: {s}\n", .{ i, e });
    }

    var demo_file = try std.fs.cwd().openFile(file_path, .{});
    defer demo_file.close();

    const demo_size = try demo_file.getEndPos();
    std.debug.print("File open: size {d:.2} Mb\n", .{@as(f64, @floatFromInt(demo_size)) / (1024.0 * 1024.0)});

    const demo_content = try allocator.alloc(u8, demo_size);

    const read_size = try demo_file.read(demo_content);
    std.debug.print("Total read bytes: {d}\n", .{read_size});

    const header: DemoHeader = .{
        .header = demo_content[0..8],
        .demo_protocol = readInt(demo_content, 8),
        .network_protocol = readInt(demo_content, 12),
        .server_name = readString(demo_content, 16, 260),
        .client_name = readString(demo_content, 276, 260),
    };
    header.print();
}

test "simple test" {}
